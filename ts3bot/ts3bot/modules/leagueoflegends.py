"""League of Legends module based around riot API to fetch LoL-related info"""
import logging, re
import requests

import pprint

INVALID_MSG = 'Invalid command. See !lol for a list of valid commands'
NAME_MAX_LENGTH = 16

#regions for riot API
REGIONS = ['na', 'euw', 'eune', 'br', 'kr', 'lan', 'las', 'oce', 'ru', 'tr']
#Regions for riot API translated to current-game API. Different, for some reason
LIVE_REGIONS = {
                    'na':'NA1', 'euw':'EUW1', 'eune':'EUN1', 'br':'BR1', 'kr':'KR',
                    'oce':'OC1', 'lan':'LA1', 'las':'LA2', 'tr':'TR1', 'ru':'RU'
               }
CHAMPION_ALIAS = {
                    'urgod':'urgot', 'donger':'heimerdinger',
                    'rito':'tryndamere', 'tomcrunch':'tahmkench',
                    'chloeprice':'yorick', 'riot':'tryndamere',
                    'poopy':'poppy'
                 }

_api_region = None
_api_key = None
_api_url = None

_summoners = {}
_champions = None
_champions_by_id = None

def setup(phenny, config):
    """Load champion ids for champion_info"""
    if hasattr(config, 'LOL_API_REGION') and hasattr(config, 'LOL_API_KEY'):
        global _api_region, _api_key
        _api_region = config.LOL_API_REGION
        _api_key = config.LOL_API_KEY
    else:
        return False

    global _api_url
    _api_url = 'https://%s.api.pvp.net'
    api_champion_path = '/api/lol/static-data/oce/v1.2/champion?dataById=%s&api_key=' + _api_key
    url = _api_url % 'oce'
    response = requests.get(url + api_champion_path % 'false')

    if response:
        global _champions
        _champions = response.json()['data']
    else:
        logging.warning('Could not load champion info')
        return False

    response = requests.get(url + api_champion_path % 'true')

    if response:
        global _champions_by_id
        _champions_by_id = response.json()['data']
    else:
        logging.warning('Could not load champion info by id')
        return False

    return True

def on_match_info(event):
    """Fetch match info using riot api"""
    msg = event.parsed[0]['msg'].split(' ', 2)
    logging.debug(msg)
    if len(msg) == 0:
        logging.warning('Somehow called on_match_info with zero arguments')
        return None
    elif len(msg) == 1 or len(msg) == 2:
        #explain command usage
        return 'Usage: matchinfo [region] [summoner name] (without brackets)'
    elif len(msg) == 3:
        #run command
        return get_match_info(msg[1], msg[2])
on_match_info.commands = ['matchinfo']

def on_champion_info(event):
    """Fetch static champion info using riot api"""
    msg = event.parsed[0]['msg'].split(' ')
    logging.debug(msg)
    if len(msg) == 0:
        logging.warning('Somehow called on_champion_info with zero arguments')
        return None
    elif len(msg) == 1:
        return 'Usage: champion [champion name without spaces] \
                [ability letter (optional)] (without brackets)'
    elif len(msg) == 2:
        return get_champion_info(msg[1])
    elif len(msg) == 3:
        return get_champion_info(msg[1], msg[2])
    else:
        return 'Usage: champion [champion name without spaces] \
                [ability letter (optional)] (without brackets)'
on_champion_info.commands = ['champion']

def get_match_info(region, name):
    """Get match info of player 'name' in region 'region'"""
    region = region.lower()
    if not region in REGIONS:
        return 'Invalid region. Valid regions:\n' + str(REGIONS)

    id = get_summoner_id(region, name)
    if not id:
        return 'Summoner name %s (%s) is not valid.' % (name, region)

    info = request_match_info(region, id)
    if info:
        #TODO: nice printing of game type
        try:
            m, s = divmod(info['gameLength'], 60)
            type = '[b][u]%s - %d:%02d[/u][/b]' % (info['gameMode'], m, s)
            team1, team2 = get_participants(info['participants'])
        except KeyError as e:
            logging.exception('Missing keys in match info ' + str(e))
            return 'Incomplete riot data'

        #Minimize calls to riot API due to call limit
        ids = []
        for player in team1:
            ids.append(player[1])
        for player in team2:
            ids.append(player[1])

        ranks = get_summoner_ranks(region, ids)
        # Build strings
        matchinfo = [type]
        t1info = build_team('[b][color=blue]BLUE[/color][/b]:', team1, region, ranks)
        matchinfo.append('\n'.join(t1info))

        t2info = build_team('[b][color=purple]PURPLE[/color][/b]:', team2, region, ranks)
        matchinfo.append('\n'.join(t2info))

        return matchinfo

    else:
        return 'Summoner %s (%s) is not in a match.' % (name, region)

def build_team(team_name, player_list, region, ranks):
    formatstr = '[b]Name[/b]: [url=%s]%s[/url]  |  [b]Champion[/b]: %s  |  [b]Rank[/b]: %s'
    profile_base_url = 'http://' + region + '.op.gg/summoner/userName='
    tinfo = [team_name]
    for i, player in enumerate(player_list):
        profile_url = profile_base_url + str(player[0]).replace(' ', '+')

        rank = None
        idstr = str(player[1])
        if idstr in ranks:
            rank = ranks[idstr]
        else:
            rank = 'None'

        try:
            champion_name = _champions_by_id[str(player[2])]['name']
            tinfo.append(formatstr % (profile_url, player[0], champion_name, rank))
        except KeyError as e:
            logging.exception('Missing key in champions_by_id ' + str(e))
            tinfo.append(formatstr % (profile_url, player[0], player[2], rank))

    return tinfo

def get_champion_info(name, ability=None):
    """Get champion info"""
    name = name.lower()

    alias = case_insensitive_search(name, CHAMPION_ALIAS.keys())
    #Check for keys, ignoring case sensitivity
    if alias:
        name = CHAMPION_ALIAS[alias]

    key = case_insensitive_search(name, _champions.keys())
    if key:
        msgs = []
        msgs.append('[u][b]%s %s[/b][/u]: ' % (_champions[key]['name'], 
                                        _champions[key]['title']))
        id = _champions[key]['id']
        info = request_champ_info(id)

        if info:
            if (not ability or ability.lower() == 'passive') and 'passive' in info.keys():
                passive = info['passive']['sanitizedDescription']
                msgs.append('[b]Passive[/b]: ' + passive)
            if 'spells' in info.keys() and (not ability or ability.lower() != 'passive'):
                spells = info['spells']
                parsed = parse_champ_spells(spells, ability)
                if parsed:
                    msgs.extend(parsed)
        if ability:
            return '\n'.join(msgs)
        else:
            return msgs
    else:
        return 'Champion does not exist'

def get_summoner_id(region, name):
    """Get summoner id from name"""
    #Riot strips whitespace and makes names lowercase
    name = re.sub(r'\s+', '', name.lower())
    if region in REGIONS and len(name) <= NAME_MAX_LENGTH:
        key = region + name

        if key in _summoners:
            return _summoners[key]
        else:
            data = request_summoner_info(region, name)
            if data:
                _summoners[key] = data[name]['id']
                return _summoners[key]
    return None

def get_summoner_ranks(region, ids = []):
    """Get summoner soloq rank from region, id"""
    response = request_summoner_rank(region, ids)
    if response:
        ranks = {}
        for key in response.keys():
            rankedinfo = response[key][0]
            if rankedinfo['queue'] == 'RANKED_SOLO_5x5':
                 ranks[key] = rankedinfo['tier'] + ' ' + rankedinfo['entries'][0]['division']
            else:
                continue
        return ranks
    else:
        return None

def get_participants(participants):
    """
        Takes dictionary of participants from riot API. 
        returns team1 and team2 with tuple (player name, player id, champion id
    """
    team1 = []
    team2 = []

    for participant in participants:
        player = None
        if participant['bot']:
            player = ('Bot', 0, 0)
        else:
            name = participant['summonerName']
            id = participant['summonerId']
            champ = participant['championId']
            player = (name, id, champ)

        #separate players into teams
        if participant['teamId'] == 100:
            team1.append(player)
        else:
            team2.append(player)

    return team1, team2

def request_match_info(region, summoner_id):
    """Pull match info from riot match data by region and summoner id"""
    api_live_path = '/observer-mode/rest/consumer/getSpectatorGameInfo/%s/%s?api_key=%s'

    url = _api_url % region + api_live_path % (LIVE_REGIONS[region], summoner_id, _api_key)
    response = requests.get(url)

    #TODO: handle different header info
    if response.status_code == 200:
        return response.json()
    else:
        return None

def request_summoner_info(region, name):
    """Get summoner info from riot summoner info by region and name"""
    api_search_path = '/api/lol/%s/v1.4/summoner/by-name/%s?api_key=' + _api_key
    url = _api_url % region + api_search_path % (region, name)
    response = requests.get(url)

    #TODO: handle different header info
    if response.status_code == 200:
        return response.json()
    else:
        return None

def request_champ_info(id):
    """Pull champion info from riot static data"""
    api_champdata_path = '/api/lol/static-data/oce/v1.2/champion/%s?champData=passive,spells&api_key=' + _api_key
    response = requests.get(_api_url % 'oce' + api_champdata_path % id)
        
    if response.status_code == 200:
        return response.json()
    else:
    #TODO: handle different header info
        return None

def request_summoner_rank(region, ids):
    """Get ranked info from riot league info by region and id"""
    api_league_path = '/api/lol/%s/v2.5/league/by-summoner/%s/entry?api_key=' + _api_key
    idstr = ','.join(str(x) for x in ids)
    response = requests.get(_api_url % region + api_league_path % (region, idstr))

    if response.status_code == 200:
        return response.json()
    else:
        #TODO: handle different header info
        return None

"""
    TODO: implement !help
    leagueoflegends.py isn't endorsed by Riot Games and doesn't reflect
    the views or opinions of Riot Games or anyone officially involved in 
    producing or managing League of Legends. League of Legends and Riot 
    Games are trademarks or registered trademarks of Riot Games, Inc. 
    League of Legends � Riot Games, Inc.
"""
def parse_champ_spells(spells, ability):
    """Parse spell info from riot API"""
    SPELL_KEYS = ['Q', 'W', 'E', 'R']
    SPELL_DICT = {'q':0, 'w':1, 'e':2, 'r':3}
    spell_text = []

    if ability:
        ability = ability.lower()
        if ability.upper() in SPELL_KEYS:
            built = spell_builder(spells[SPELL_DICT[ability]], ability.upper())
            spell_text.append(built)
        else:
            spell_text.append(str(ability) + ' is not a valid spell')
    else:
        for i, spell in enumerate(spells):
            spell_text.append(spell_builder(spell, SPELL_KEYS[i]))

    
    return spell_text

def spell_builder(spell, ability):
    """Build spell description from spell information"""
    try:
        name = spell['name']
        tooltip = spell['sanitizedTooltip']
        logging.debug(tooltip)

        reg = r'{{\s([a-z][0-9][0-9]?)\s}}'

        # Find all variables
        matches = re.findall(reg, tooltip)

        spellvars = []
        for match in matches:
            if match[0] == 'e':
                coeff = spell['effectBurn'][int(match[1:])]
                if coeff:
                    spellvars.append(coeff)
                else:
                    spellvars.append('Missing {{ %s }}' % match)
            elif match[0] == 'a':
                # check to see if first key is a1. if not, search for entry with key
                try:
                    vars = None
                    vars = spell['vars'][int(match[1:])-1]

                    #incorrect key
                    if not vars['key'] == match:
                        vars = search_key(spell['vars'], match)
                except IndexError as e:
                    logging.info('Missing var %s from json, using search', match)
                    vars = search_key(spell['vars'], match)
                except KeyError as e:
                    logging.exception('Missing data in json: ' + e)

                if vars:
                    spellvars.append(str(vars['coeff'][0]) + ' ' + link_to_txt(vars['link']))
                else:
                    spellvars.append('Missing {{ %s }}' % match)

            elif match[0] == 'f':
                #some of riot's data is marked f and mixed with a in vars
                vars = search_key(spell['vars'], match)
                if vars:
                    spellvars.append(str(vars['coeff'][0]) + ' ' + link_to_txt(vars['link']))
                else:
                    spellvars.append('Missing {{ %s }}' % match)
            else:
                spellvars.append('unhandled var (thanks rito)')
            
        tooltip = tooltip.replace('%', '%%')
        newtip = re.sub(reg, '[b]%s[/b]', tooltip) % tuple(spellvars)
        return '[b]%s (%s)[/b]: %s' % (name, ability, newtip)

    except KeyError as e:
        logging.exception('Missing tooltips: ' + str(e))
        return '[b]%s[/b]: Missing spell tooltip' % ability

def case_insensitive_search(search, listitems):
    """Returns item from list if matches search. Returns none if no matches"""
    #TODO: optimize
    try:
        for item in listitems:
            if search.lower() == item.lower():
                return item
            continue
        return None
    except AttributeError as e:
        logging.exception(e)
        return None

def search_key(lst, key):
    """slow search for keys through riot api"""
    for item in lst:
        if 'key' in item and key == item['key']:
            return item

    #could not find key
    logging.warning('Missing key %s from riot data: %s', key, lst)
    return None           

def link_to_txt(link):
    """Turns link var in spell data to something readable"""
    if link == 'bonusattackdamage' or link == '@dynamic.bonusattackdamage':
        return 'Bonus AD'
    elif link == 'attackdamage' or link == '@dynamic.attackdamage':
        return 'AD'
    elif link == 'spelldamage' or link == '@dynamic.abilitypower':
        return 'AP'
    else:
        return link
