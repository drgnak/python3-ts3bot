"""Get random power from powerlisting wikia"""
from mechanicalsoup import Browser
from datetime import datetime
import re, logging
BASE_URL = 'http://powerlisting.wikia.com/wiki/'
URL = 'http://powerlisting.wikia.com/wiki/Special:Random'
SUPERPOWER_RESPONSE = '{}: [url={}]{}[/url] - {}'

browser = None
today = None
cached_powers = {}


def setup(phenny, config):
    global browser, today
    browser = Browser()
    today = datetime.today().replace(hour=0, minute=0, second=0, microsecond=0)
    logging.debug(today)
    if browser:
        return True
    else:
        return False

def on_superpower(event):
    """Get a random superpower"""
    global cached_powers, today
    invoker = event.parsed[0]['invokername']
    msg = event.parsed[0]['msg'].split(' ', 1)
    if len(msg) == 1:
        # Check if today is a new day so the user can get a new power
        if (datetime.today() - today).days != 0:
            cached_powers.clear()
            today = datetime.today().replace(hour=0, minute=0, second=0, microsecond=0)
        try:
            power, text = cached_powers[invoker]
            logging.debug('Cached power found: ' + power)
        except KeyError:
            logging.debug('No cached powers found. Looking for a random power.')
            page = browser.get(URL)
            power = page.soup.title.get_text().split(' | ')[0]
            power = power.replace(' ', '_')
            text = get_superpower_from_page(page)
            # Cache user's power for today
            cached_powers[invoker] = (power, text)
            logging.debug(cached_powers)
        return SUPERPOWER_RESPONSE.format(invoker + ': Your power for today' , BASE_URL + power, power, text)
    elif len(msg) > 1:
        power = msg[1].title()
        power = power.replace(' ', '_')
        logging.debug('Fetching: ' + BASE_URL + power)
        page = browser.get(BASE_URL + power)
        if page.status_code == 404:
            return 'Power does not exist'
        return SUPERPOWER_RESPONSE.format(invoker, BASE_URL + power, power, get_superpower_from_page(page))
#    return event.parsed[0]['invokername'] + ': [url=' + BASE_URL + title + ']' + title + '[/url] - ' + 
on_superpower.commands = ['superpower']

def get_superpower_from_page(page):
    search = page.soup.find('div', {'class':'mw-content-ltr mw-content-text'})
    try:
        text = search.find('p', recursive=False).get_text()
    except AttributeError as e:
        logging.debug(search.get_text())
        logging.exception('Missing appropriate formatting for text %s', str(e))
        search.table.extract()
        search.div.extract()
        text = search.get_text()
    return text.replace('\xa0', ' ')
