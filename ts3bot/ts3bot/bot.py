import ts3
from ts3bot import loader
from ts3bot import worker
import glob, logging, os, time
from threading import Semaphore, Thread
from pprint import pformat
from queue import Queue

class Bot:
    """ 
       Threaded bot for ts3. Parses incoming messages
       and sends them to the appropriate module 
       config: config object containing settings
    """
    def __init__(self, config):
        self.obuffer = Queue()
        self.nick = config.NICK
        self.user = config.USERNAME
        self.passwd = config.USER_PASSWD

        if hasattr(config, 'FLOOD_TIME') and hasattr(config, 'FLOOD_COMMANDS'):
            self.flood_protection = True
            self.flood_time = config.FLOOD_TIME
            self.flood_commands = config.FLOOD_COMMANDS
            self.cmd_counter = 0
            self.last_cmd_time = 0
        else:
            self.flood_protection = False

        self.pool = worker.ThreadPool()
        self.sem = Semaphore(4)

        #default help command
        self.commands = {config.TRIGGER + 'help':self.help}
        self.load_modules(config)
        self.connect(config.SERVER, config.PORT)
        if self.conn.is_connected():
            self.join_vserver(config.VSERVER_ID)
            self.set_nick(config.NICK)
            self.join_channel(config.CHANNEL_ID)
            #self.say('Hi. I\'m a bot')
        else:
            logging.error(self.conn.last_resp)


    def connect(self, server, port):
        """Connect to server:port with serverquery account"""
        self.conn = ts3.query.TS3Connection(server, port)
        try:
            self.conn.login(client_login_name=self.user, 
                            client_login_password=self.passwd)
            self.conn.keepalive()
            #logging.info('Connected to %s:%s', (server, port))
        except ts3.query.TS3QueryError as e:
            logging.exception('Login failed: %s', e.resp.error['msg'])
            exit(1)

    def set_nick(self, nick):
        """Set bot's nickname. Must be in a vserver"""
        if hasattr(self, 'sid'):
            self.conn.clientupdate(client_nickname=nick)
            if self.flood_protection:
                self.update_flood_counter()
            logging.debug("Set name to %s", nick)
        else:
            logging.warning('Cannot set nick before joining a server')

    def join_vserver(self, sid):
        """Join vserver with sid and register text events for bot to respond to"""
        try:
            self.conn.use(sid=sid)
            self.sid = sid
            logging.info('Joined sid: %s', sid)
            if self.flood_protection:
                self.update_flood_counter()
        except ts3.query.TS3QueryError as e:
            logging.exception('Could not join vserver: %s', e.resp.error['msg'])
            exit(1)
        else:
            self.register_events()

            # Get client ID
            client_info = self.conn.whoami()
            if self.flood_protection:
                self.update_flood_counter()
            logging.debug(client_info[0])
            self.clid = client_info[0]['client_id']
            self.cid = client_info[0]['client_channel_id']

    def load_modules(self, config):
        """Get modules from path and pass paths to load_module"""
        paths = glob.glob(config.MODULE_PATH + '/*.py')
        logging.debug(paths)
        for path in paths:
            if os.path.exists(path):
                mod, commands = loader.load_module(self, path, config)
                if mod and commands:
                    self.register_module(mod, config.TRIGGER, commands)
            else:
                logging.warning('Tried to load a module that doesn\'t exist: %s',
                                path)
                continue

    def register_module(self, module, trigger, commands):
        """Register module commands with trigger. Checks for duplicate entries"""
        for func, command_list in commands:
            if isinstance(command_list, list):
                for command in command_list:
                    # command with trigger
                    logging.debug('Registering command "%s"', command)
                    com = trigger + command
                    # Bind command to function
                    if com not in self.commands:
                        self.commands[com] = func
                        logging.info('Registered command %s, %s', 
                                     command, module.__name__)
                    else:
                        logging.warning('Duplicate trigger .%s in %s. \
                                        Function %s is registered. Ignored',
                                        com, func, self.commands[com])

    def join_channel(self, cid, passwd=None):
        """Join a channel with cid and passwd. Must be in a vserver"""
        if hasattr(self, 'sid') and not cid == self.cid:
            try:
                self.conn.clientmove(clid=self.clid, cid=cid,cpw=passwd)
                if self.flood_protection:
                    self.update_flood_counter()
            except ts3.query.TS3QueryError as e:
                logging.exception('Could not join channel: %s', e.resp.error['msg'])
        else:
            logging.warning('Tried to join a channel before joining a server')


    def register_events(self):
        """
            Register text events
            TODO: register PMs
        """
        self.conn.on_event.connect(self.recv_event)
        self.conn.servernotifyregister(event='textchannel')
        self.conn.recv_in_thread()
        if self.flood_protection:
            self.update_flood_counter(3)

    def recv_event(self, sender, event):
        """Text event handler. TODO: write to output buffer"""
        try:
            if len(event.parsed) > 1:
                logging.info('Multiple parsed ' + len(event.parsed))
            parsed = event.parsed[0]
            if 'invokerid' in parsed and not parsed['invokerid'] == self.clid:
                if event.event == 'notifytextmessage':
                    self.on_notifytextmessage(sender, event)
                logging.debug(event.event + ' ' + pformat(parsed))
        except Exception as e:
            logging.exception('Error while parsing event: %s: %s', parsed, e)

    def on_notifytextmessage(self, sender, event):
        """Handle channel text events"""
        #get first word of message
        first = event.parsed[0]['msg'].split(' ', 1)[0]
        if first in self.commands:
            logging.debug(first)
            obj = self.commands[first]
            threaded = True
            if hasattr(obj, 'threaded') and obj.threaded == False:
                threaded = False

            if threaded:
                thread = Thread(target=worker.worker, name='thread_' + first,
                                args=(obj, event, self.obuffer, self.sem, self.pool))
                thread.start()
            else:
                msg = obj(event)
                self.obuffer.put((obj, text))
            # Old say code
            #text = obj(event)
            #if text:
            #    if hasattr(obj, 'targetmode'):
            #        self.say(text, obj.targetmode, event.parsed[0]['invokerid'])
            #    else:
            #        self.say(text)



    def say(self, msg, targetmode=2, target=0):
        #TODO: functionality now overlaps worker and send_message. Merge relevants parts and 
        #implement a new function for non-threaded calls
        """Send a message to target"""
        if not hasattr(self, 'sid'):
            logging.warn('Tried to send a message before joining a server')
        #send message
        if type(msg) == str:
            self.send_message(msg, targetmode, target)
        elif type(msg) == list:
            for text in msg:
                self.send_message(text, targetmode, target)
                time.sleep(1)
            
    def send_message(self, msg, targetmode, target):
        """Send textmessage query"""
        try:
            logging.info('Bot:say:%s', msg)
            self.conn.sendtextmessage(targetmode=targetmode, 
                                    target=target, msg=msg)
        except ts3.query.TS3QueryError as e:
            if e.resp.error['id'] == '1541':
                self.conn.sendtextmessage(targetmode=targetmode, 
                                          target=target, msg=msg[:1024])
            else:
                logging.exception('Could not send message "%s": %s',
                                  msg, e.resp.error)

        if self.flood_protection:
            self.update_flood_counter()

    def help(self, event):
        self.say('Commands: %s' % self.commands.keys())
    
    def update_flood_counter(self, num=1):
        """Add timestamp to timer"""
        if self.flood_protection:
            self.last_cmd_time = time.time()
            self.cmd_counter += num

    def process_buffer(self):
        if (not self.flood_protection or self.cmd_counter < self.flood_commands - 1):
            obj, msg = self.obuffer.get()
            if hasattr(obj, 'targetmode'):
                self.say(msg, obj.targetmode, event.parsed[0]['invokerid'])
            else:
                self.say(msg)

    def run(self):
        try:
            while True:
                #Remove expired timer
                if (self.flood_protection and self.last_cmd_time and 
                        time.time() - self.last_cmd_time > self.flood_time):
                    self.last_cmd_time = 0
                    self.cmd_counter = 0

                if self.obuffer:
                    self.process_buffer()

        except KeyboardInterrupt:
            logging.info('Keyboard interrupt recieved')
            exit(0)
