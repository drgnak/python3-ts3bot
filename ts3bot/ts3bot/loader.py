import logging, imp, os

def load_module(phenny, path, config):
    """loads and parses a module"""
    modname = os.path.splitext(os.path.basename(path))[0]
    try:
        module = imp.load_source(modname, path)
    except Exception as e:
        logging.exception('Error loading module %s: %s', modname, str(e))
        return None, None
    if hasattr(module, 'setup'):
        setup = module.setup(phenny, config)
        if not setup:
            return False

    commands = []
    for name, obj in vars(module).items():
        if hasattr(obj, 'commands'):
            commands.append((obj, obj.commands))
            logging.debug('loader:Added commands for module %s', name)
    return module, commands
