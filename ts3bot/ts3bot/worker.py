import threading
import logging

class ThreadPool(object):
    def __init__(self):
        super(ThreadPool, self).__init__()
        self.active = []
        self.lock = threading.Lock()
    def make_active(self, name):
        with self.lock:
            self.active.append(name)
            logging.debug('Running: %s', self.active)
    def make_inactive(self, name):
        with self.lock:
            self.active.remove(name)
            logging.debug('Running: %s', self.active)

def worker(func, event, queue, sem, pool):
    logging.debug('Waiting to join pool')
    with sem:
        name = threading.currentThread().getName()
        pool.make_active(name)
        msg = ''
        try:
            msg = func(event)
        except Exception as e:
            logging.exception('Failed to run function: ' + str(e))
            msg = 'Error running ' + func.__name__
            
        if msg:
            if type(msg) == list:
                for m in msg:
                    queue.put((func, m))
            else:
                queue.put((func, msg))
        pool.make_inactive(name)
