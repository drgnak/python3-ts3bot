import logging, threading
import sys, re, time
from ts3bot.bot import Bot

def run(config):
    logger = logging.getLogger()
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    logger.setLevel(logging.DEBUG)

    # console logging
    ch = logging.StreamHandler(stream=sys.stdout)
    ch.setLevel(logging.DEBUG)
    ch.setFormatter(formatter)
    logger.addHandler(ch)

    # file logging
    fh = logging.FileHandler('debug.log')
    fh.setLevel(logging.DEBUG)
    fh.setFormatter(formatter)
    logger.addHandler(fh)
    

    bot = Bot(config)
    
    try:
        bot.run()
    except Exception as e:
        logging.critical("Bot failed: %s", e)
        exit(1)


